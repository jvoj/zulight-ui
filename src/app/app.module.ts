import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MainComponent } from './main/main.component';
import {IMqttServiceOptions, MqttModule} from 'ngx-mqtt';
import { ControlComponent } from './control/control.component';
import {MatButtonModule, MatCardModule, MatSliderModule} from '@angular/material';
import {ColorPickerModule} from 'ngx-color-picker';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: 'm21.cloudmqtt.com',
  port: 33131,
  path: '',
  protocol: 'wss',
  username: 'svetlo',
  password: 'svetlo'
};

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ControlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    ColorPickerModule,
    MatSliderModule,
    FormsModule,
    ReactiveFormsModule,
    MqttModule.forRoot(MQTT_SERVICE_OPTIONS)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
