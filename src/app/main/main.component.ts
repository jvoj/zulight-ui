import { Component, OnInit } from '@angular/core';
import {MyMqttService} from '../my-mqtt.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  constructor(
    private mqtt: MyMqttService
  ) { }

  ngOnInit() {
    this.mqtt.connect();
  }

  clicked() {
    this.mqtt.publish('ahoj', 'nahoj');
  }

}
