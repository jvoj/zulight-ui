import { Injectable } from '@angular/core';

import {Observable, Subscription} from 'rxjs';

import {
  IMqttMessage,
  MqttService
} from 'ngx-mqtt';


@Injectable({
  providedIn: 'root'
})
export class MyMqttService {

  private subscription: Subscription;
  public message: string;

  constructor(private mqttService: MqttService) {
    this.subscription = this.mqttService.observe('my/topic').subscribe((message: IMqttMessage) => {
      this.message = message.payload.toString();
      console.log({gotMessage: this.message});
    });
  }

  public publish(topic: string, message: string): void {
    // this.mqttService.unsafePublish(topic, message, {qos: 1, retain: false});
    this.mqttService.publish(topic, message, {qos: 0, retain: false}).subscribe();
  }

  public subscribe(topic: string): Observable<IMqttMessage> {
    return this.mqttService.observe(topic);
  }

  connect() {

  }
}
