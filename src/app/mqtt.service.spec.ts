import { TestBed } from '@angular/core/testing';

import { MyMqttService } from './mqtt.service';

describe('MqttService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyMqttService = TestBed.get(MyMqttService);
    expect(service).toBeTruthy();
  });
});
