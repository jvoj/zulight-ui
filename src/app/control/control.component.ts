import {Component, Input, OnInit} from '@angular/core';
import {MyMqttService} from '../my-mqtt.service';

@Component({
  selector: 'app-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss']
})
export class ControlComponent implements OnInit {

  @Input() public mac: string;
  @Input() public caption: string;

  private prefix: string;

  private color: string;

  private brightness = 0;

  public message = '';
  public loading = false;

  constructor(
    private mqtt: MyMqttService
  ) { }

  ngOnInit() {
    this.prefix = 'dev/' + this.mac + '/';
    this.mqtt.subscribe(this.prefix + '#').subscribe(value => {
      this.message = value.payload.toString();
      // if (this.prefix + 'white') {
      //   this.message = 'White done...';
      // } else if (this.prefix + 'off') {
      //   this.message = 'White done...';
      // }
      // console.log({value});
    });
  }

  on() {
    this.mqtt.publish(this.prefix + 'control/white', '');
  }

  off() {
    this.mqtt.publish(this.prefix + 'control/off', '');
  }

  rainbow() {
    this.mqtt.publish(this.prefix + 'control/rainbow', '');
  }

  green() {

  }

  changedColor() {
    console.log(this.color);

    this.mqtt.publish(this.prefix + 'control/rgb', this.color);
  }

  brightnessChange() {
    console.log(this.brightness);
    this.mqtt.publish(this.prefix + 'control/brightness', this.brightness.toString());
  }
}
